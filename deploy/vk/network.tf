resource "vkcs_networking_network" "net" {
  name           = "${var.name}-net"
  admin_state_up = true
}

resource "vkcs_networking_subnet" "compute" {
  name       = "compute"
  network_id = vkcs_networking_network.net.id
  cidr       = "10.0.0.0/24"
}

resource "vkcs_networking_subnet" "db" {
  name            = "db"
  network_id      = vkcs_networking_network.net.id
  cidr            = "10.0.16.0/24"
}

data "vkcs_networking_network" "ext" {
  name = "ext-net"
}

resource "vkcs_networking_router" "net" {
  name                = "${var.name}-router"
  admin_state_up      = true
  external_network_id = data.vkcs_networking_network.ext.id
}

resource "vkcs_networking_router_interface" "compute" {
  router_id = vkcs_networking_router.net.id
  subnet_id = vkcs_networking_subnet.compute.id
}

resource "vkcs_networking_router_interface" "db" {
  router_id = vkcs_networking_router.net.id
  subnet_id = vkcs_networking_subnet.db.id
}