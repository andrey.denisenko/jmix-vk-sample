data "vkcs_compute_flavor" "main_db" {
  name = var.main_db_flavor
}

resource "random_password" "main_db" {
  count   = var.main_db_random_password ? 1 : 0
  length  = 16
  special = false
}

locals {
  main_db_host = vkcs_networking_port.main_db.all_fixed_ips[0]
  main_db_port = "3306"
  main_db_password = var.main_db_random_password ? random_password.main_db[0].result : var.main_db_password
}

resource "vkcs_db_instance" "main_db" {
  name = "${var.name}-main-db"

  datastore {
    type    = var.main_db_engine
    version = var.main_db_engine_version
  }

  keypair = vkcs_compute_keypair.key.id

  flavor_id   = data.vkcs_compute_flavor.main_db.id
  size        = 8
  volume_type = "ceph-ssd"
  disk_autoexpand {
    autoexpand    = true
    max_disk_size = 1000
  }

  network {
    port = vkcs_networking_port.main_db.id
  }
}

resource "vkcs_networking_secgroup" "main_db" {
  name = "${var.name}-main-db-secgroup"
}

resource "vkcs_networking_secgroup_rule" "main_db" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = local.main_db_port
  port_range_max    = local.main_db_port
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.main_db.id
}

resource "vkcs_networking_port" "main_db" {
  name           = "${var.name}-port-main-db"
  network_id     = vkcs_networking_network.net.id
  admin_state_up = true
  security_group_ids = [ vkcs_networking_secgroup.main_db.id ]
  fixed_ip {
    subnet_id = vkcs_networking_subnet.db.id
  }
}

resource "vkcs_db_database" "main_db" {
  name    = var.main_db_name
  dbms_id = vkcs_db_instance.main_db.id
  charset = "utf8"
  collate = "utf8_general_ci"
}

resource "vkcs_db_user" "db-user" {
  name     = var.main_db_user
  password = local.main_db_password

  dbms_id = vkcs_db_instance.main_db.id

  databases = [vkcs_db_database.main_db.name]
}