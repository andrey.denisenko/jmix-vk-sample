terraform {
  required_providers {
    vkcs = {
      source = "vk-cs/vkcs"
    }
  }
}

provider "vkcs" {
  username = var.vkcs_username
  password = var.vkcs_password
  project_id = var.vkcs_project_id
  region = var.vkcs_region
}